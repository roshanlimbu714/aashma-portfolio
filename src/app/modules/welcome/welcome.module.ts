import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WelcomeLayoutComponent} from './welcome-layout/welcome-layout.component';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {MyWorksComponent} from './my-works/my-works.component';
import {PortfolioComponent} from './portfolio/portfolio.component';
import {ContactComponent} from './contact/contact.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [
    WelcomeLayoutComponent,
    HomeComponent,
    AboutComponent,
    MyWorksComponent,
    PortfolioComponent,
    ContactComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class WelcomeModule {
}
