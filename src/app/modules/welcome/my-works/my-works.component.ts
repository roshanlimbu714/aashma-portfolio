import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-my-works',
  templateUrl: './my-works.component.html',
  styleUrls: ['./my-works.component.scss']
})
export class MyWorksComponent implements OnInit {
  mockData: any[] = [
    {title: 'John doe', subtitle: '12lorem asdfasdf'},
    {title: 'John doe2', subtitle: 'l3orem asdfasdf'},
    {title: 'John doe3', subtitle: 'lo4rem asdfasdf'},
    {title: 'John doe4', subtitle: 'lo5rem asdfasdf'},
    {title: 'John doe5', subtitle: 'lorem asdfasdf'},
    {title: 'John doe7', subtitle: 'lo6rem asdfasdf'},
    {title: 'John do7e8', subtitle: 'l87orem asdfasdf'},
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
