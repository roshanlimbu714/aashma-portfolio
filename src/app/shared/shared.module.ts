import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project/project.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {MaterialModule} from './material/material.module';

@NgModule({
  declarations: [ProjectComponent, NavBarComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    NavBarComponent,
    ProjectComponent
  ]
})
export class SharedModule { }
