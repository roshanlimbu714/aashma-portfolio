import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  open = false;

  constructor() {
  }

  navOpen(a) {
    const menubar = document.getElementById('menu-bar');
    this.open = a;
    if (a === true) {
      menubar.classList.remove('close');
      menubar.classList.add('open');
    } else {
      menubar.classList.remove('open');
      menubar.classList.add('close');
    }
  }

  ngOnInit() {
  }

}
