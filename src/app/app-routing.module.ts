import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WelcomeLayoutComponent} from './modules/welcome/welcome-layout/welcome-layout.component';

const routes: Routes = [
  {path: '', component: WelcomeLayoutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
